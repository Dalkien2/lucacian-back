package org.lucacian.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.lucacian.entity.Proveedor;
import org.lucacian.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/Proveedor")
@Slf4j
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,
        RequestMethod.POST,
        RequestMethod.OPTIONS,
        RequestMethod.PUT })
public class ProveedorController {
    private final ProveedorService proveedorService;

    @Autowired
    public ProveedorController(
                               ProveedorService proveedorService) {
        this.proveedorService = proveedorService;
    }

    @GetMapping("")
    public List<Proveedor> getProveedors() {
        log.info("process=get-proveedors");
        return this.proveedorService.getAllProveedors();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Proveedor> getProveedor(@PathVariable Long id) {
        log.info("process=get-proveedor, proveedor_id={}", id);
        Optional<Proveedor> proveedor = this.proveedorService.getProveedorById(id);
        return proveedor.map( u -> ResponseEntity.ok(u))
                   .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("")
    @ResponseStatus(CREATED)
    public Proveedor createProveedor(@RequestBody Proveedor proveedor) {
        return this.proveedorService.createProveedor(proveedor);
    }

    @PutMapping("/{id}")
    @ResponseStatus(ACCEPTED)
    public Proveedor updateProveedor(@PathVariable Long id, @RequestBody Proveedor proveedor) {
        return this.proveedorService.updateProveedor(proveedor);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(OK)
    public void deleteProveedor(@PathVariable Long id) {
        this.proveedorService.deleteProveedor(id);
    }

}
