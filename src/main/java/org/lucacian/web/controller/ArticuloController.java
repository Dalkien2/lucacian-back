package org.lucacian.web.controller;

import org.lucacian.entity.Articulo;
import org.lucacian.entity.Cliente;
import org.lucacian.entity.Factura;
import org.lucacian.entity.Articulo;
import org.lucacian.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/Articulo")
@Slf4j
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,
        RequestMethod.POST,
        RequestMethod.OPTIONS,
        RequestMethod.PUT })
public class ArticuloController {

    private final ArticuloService Articuloservice;

    @Autowired
    public ArticuloController(ArticuloService Articuloservice) {
        this.Articuloservice = Articuloservice;
    }

    @GetMapping("")
    public List<Articulo> getArticulos() {
        return this.Articuloservice.getAllArticulos();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Articulo> getArticulo(@PathVariable Long id) {
        Optional<Articulo> articulo = this.Articuloservice.getArticuloById(id);
        return articulo.map( u -> ResponseEntity.ok(u))
                   .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("")
    @ResponseStatus(CREATED)
    public Articulo createArticulo(@RequestBody Articulo articulo) {
        return this.Articuloservice.createArticulo(articulo);
    }

    @PutMapping("/{id}")
    @ResponseStatus(ACCEPTED)
    public Articulo updateArticulo(@PathVariable Long id, @RequestBody Articulo articulo) {
        return this.Articuloservice.updateArticulo(articulo);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(OK)
    public void deleteArticulo(@PathVariable Long id) {
        this.Articuloservice.deleteArticulo(id);
    }
}
