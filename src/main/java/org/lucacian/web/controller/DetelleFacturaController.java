package org.lucacian.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.lucacian.entity.DetalleFactura;
import org.lucacian.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/DetalleFactura")
@Slf4j
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,
        RequestMethod.POST,
        RequestMethod.OPTIONS,
        RequestMethod.PUT })
public class DetelleFacturaController {

    private final DetalleFacturaService detalleFacturaService;

    @Autowired
    public DetelleFacturaController(DetalleFacturaService detalleFacturaService) {
        this.detalleFacturaService = detalleFacturaService;
    }

    @GetMapping("")
    public List<DetalleFactura> getDetalleFacturas() {
        return this.detalleFacturaService.getAllDetalleFacturas();
    }

    @GetMapping("/{idFactura}")
    public List<DetalleFactura> getDetalleFactura(@PathVariable int idFactura) {
        return this.detalleFacturaService.getDetalleFacturaById(idFactura);
    }

    @PostMapping("")
    @ResponseStatus(CREATED)
    public DetalleFactura createDetalleFactura(@RequestBody DetalleFactura detalleFactura) {
        return this.detalleFacturaService.createDetalleFactura(detalleFactura);
    }

    @PutMapping("/{id}")
    public DetalleFactura updateDetalleFactura(@PathVariable Long id, @RequestBody DetalleFactura detalleFactura) {
        return this.detalleFacturaService.updateDetalleFactura(detalleFactura);
    }

    @DeleteMapping("/{idFactura}/{idDetalle}")
    public void deleteDetalleFactura(@PathVariable int idFactura,
                                     @PathVariable int idDetalle,
                                     @RequestBody DetalleFactura detalleFactura) {
        this.detalleFacturaService.deleteDetalleFactura(detalleFactura);
    }

}
