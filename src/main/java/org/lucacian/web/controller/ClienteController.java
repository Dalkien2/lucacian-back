package org.lucacian.web.controller;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.lucacian.entity.Cliente;
import org.lucacian.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/Cliente")
@Slf4j
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,
        RequestMethod.POST,
        RequestMethod.OPTIONS,
        RequestMethod.PUT })
public class ClienteController {
    
    private final ClienteService clienteService;

    @Autowired
    public ClienteController(ClienteService clienteService
                             ) {
        this.clienteService = clienteService;
    }

    @GetMapping("")
    public List<Cliente> getClientes() {
        return  this.clienteService.getAllClientes();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Cliente> getCliente(@PathVariable Long id) {
        Optional<Cliente> cliente = this.clienteService.getClienteById(id);
        return cliente.map( u -> ResponseEntity.ok(u))
                   .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("")
    @ResponseStatus(CREATED)
    public Cliente createCliente(@RequestBody Cliente cliente) {
        System.out.println(new Gson().toJson(cliente));
        return this.clienteService.createCliente(cliente);
    }

    @PutMapping("/{id}")
    @ResponseStatus(ACCEPTED)
    public Cliente updateCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
        return this.clienteService.updateCliente(cliente);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(OK)
    public void deleteCliente(@PathVariable Long id) {
        this.clienteService.deleteCliente(id);
    }
}
