package org.lucacian.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.lucacian.entity.Factura;
import org.lucacian.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/Factura")
@Slf4j
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,
        RequestMethod.POST,
        RequestMethod.OPTIONS,
        RequestMethod.PUT })
public class FacturaController {

    private final FacturaService facturaService;

    @Autowired
    public FacturaController(FacturaService facturaService) {
        this.facturaService = facturaService;
    }

    @GetMapping("")
    public List<Factura> getFacturas() {
        log.info("process=get-facturas");
        return this.facturaService.getAllFacturas();
    }

    @GetMapping("/{id}")
    public List<Factura> getFactura(@PathVariable int id) {
        return this.facturaService.getFacturaById(id);
    }

    @PostMapping("")
    @ResponseStatus(CREATED)
    public Factura createFactura(@RequestBody Factura factura) {
        return this.facturaService.updateFactura(factura);
    }

    @PutMapping("/{id}")
    public Factura updateFactura(@PathVariable Long id, @RequestBody Factura factura) {
        return this.facturaService.updateFactura(factura);
    }

    @DeleteMapping("/{id}")
    public void deleteFactura(@PathVariable Long id) {
        this.facturaService.deleteFactura(id);
    }

}
