package org.lucacian.service;

import org.lucacian.entity.Proveedor;
import org.lucacian.repo.ProveedorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProveedorService {
    private final ProveedorRepository Repository;

    @Autowired
    public ProveedorService(ProveedorRepository Repository) {
        this.Repository = Repository;
    }

    public Optional<Proveedor> getProveedorById(Long id) {
        return Repository.findById(id);
    }

    public List<Proveedor> getAllProveedors() {
        return Repository.findAll();
    }

    public Proveedor createProveedor(Proveedor proveedor) {
        return Repository.save(proveedor);
    }

    public Proveedor updateProveedor(Proveedor proveedor) {
        return Repository.save(proveedor);
    }

    public void deleteProveedor(Long proveedorId) {
        Repository.deleteById(proveedorId);
    }
}
