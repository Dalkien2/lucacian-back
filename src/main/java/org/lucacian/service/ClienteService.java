package org.lucacian.service;

import org.lucacian.entity.Cliente;
import org.lucacian.repo.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ClienteService {
    private final ClienteRepository Repository;

    @Autowired
    public ClienteService(ClienteRepository Repository) {
        this.Repository = Repository;
    }

    public Optional<Cliente> getClienteById(Long id) {
        return Repository.findById(id);
    }

    public List<Cliente> getAllClientes() {
        return Repository.findAll();
    }

    public Cliente createCliente(Cliente cliente) {
        return Repository.save(cliente);
    }

    public Cliente updateCliente(Cliente cliente) {
        return Repository.save(cliente);
    }

    public void deleteCliente(Long clienteId) {
        Repository.deleteById(clienteId);
    }
}
