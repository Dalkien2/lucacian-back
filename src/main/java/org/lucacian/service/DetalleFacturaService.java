package org.lucacian.service;

import org.lucacian.entity.DetalleFactura;
import org.lucacian.repo.DetalleFacturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DetalleFacturaService {
    private final DetalleFacturaRepository Repository;

    @Autowired
    public DetalleFacturaService(DetalleFacturaRepository Repository) {
        this.Repository = Repository;
    }

    public List<DetalleFactura> getDetalleFacturaById(int idFactura) {
        return Repository.findByNumfactura(idFactura);
    }

    public List<DetalleFactura> getAllDetalleFacturas() {
        return Repository.findAll();
    }

    public DetalleFactura createDetalleFactura(DetalleFactura detalleFactura) {
        long val = Repository.count() + detalleFactura.getNumdetallefactura();
        detalleFactura.setNumdetallefactura((int) val);
        System.out.println(detalleFactura.getNumdetallefactura());
        return Repository.save(detalleFactura);
    }

    public DetalleFactura updateDetalleFactura(DetalleFactura detalleFactura) {
        return Repository.save(detalleFactura);
    }

    public void deleteDetalleFactura(DetalleFactura deletelleFactura) {
        Repository.delete(deletelleFactura);
    }
}
