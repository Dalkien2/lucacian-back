package org.lucacian.service;

import org.lucacian.entity.Factura;
import org.lucacian.repo.FacturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class FacturaService {
    private final FacturaRepository Repository;

    @Autowired
    public FacturaService(FacturaRepository Repository) {
        this.Repository = Repository;
    }

    public List<Factura> getFacturaById(int id) {
        return Repository.findByCifCliente(id);
    }

    public List<Factura> getAllFacturas() {
        return Repository.findAll();
    }

    public Factura createFactura(Factura factura) {
        return Repository.save(factura);
    }

    public Factura updateFactura(Factura factura) {
        return Repository.save(factura);
    }

    public void deleteFactura(Long facturaId) {
        Repository.deleteById(facturaId);
    }
}
