package org.lucacian.service;

import org.lucacian.entity.Articulo;
import org.lucacian.repo.ArticuloRepository;
import org.lucacian.repo.ArticuloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ArticuloService {
    private final ArticuloRepository Repository;

    @Autowired
    public ArticuloService(ArticuloRepository Repository) {
        this.Repository = Repository;
    }

    public Optional<Articulo> getArticuloById(Long id) {
        return Repository.findById(id);
    }

    public List<Articulo> getAllArticulos() {
        return Repository.findAll();
    }

    public Articulo createArticulo(Articulo articulo) {
        return Repository.save(articulo);
    }

    public Articulo updateArticulo(Articulo articulo) {
        return Repository.save(articulo);
    }

    public void deleteArticulo(Long articuloId) {
        Repository.deleteById(articuloId);
    }
}
