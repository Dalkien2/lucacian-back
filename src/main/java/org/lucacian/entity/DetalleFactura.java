package org.lucacian.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "detallefactura")
public class DetalleFactura implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "numfactura")
    private int numfactura;

    @Id
    @Column(name = "numdetallefactura")
    private int numdetallefactura;

    @Column(name = "cod_articulo")
    private int codArticulo;

    @Column(name = "cantidad")
    private int cantidad;

    @Column(name = "porcentaje_ganacia")
    private int porcentajeGanacia;

}
