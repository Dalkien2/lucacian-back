package org.lucacian.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "factura")
public class Factura implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "numfactura")
    private int numFactura;

    @Column(name = "fechafactura")
    private Timestamp fechaFactura;

    @Column(name = "cif_cliente")
    private int cifCliente;

}
