package org.lucacian.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "articulo")
public class Articulo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cod_articulo")
    private int codArticulo;

    @Column(name = "cif_proveedor")
    private int cifProveedor;

    @Column(name = "nombre_articulo")
    private String nombreArticulo;

    @Column(name = "caracteristica")
    private String caracteristica;

    @Column(name = "precio")
    private int precio;

}
